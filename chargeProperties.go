package entities

//ChargeProperties represents a set of properties relevant to accounting
type ChargeProperties struct {
	ID     int
	Pledge bool
	Type   string
}

//Validate validates the correctness of an Charge Properties entity against a set of rules
func (p ChargeProperties) Validate() error {
	return nil
}
