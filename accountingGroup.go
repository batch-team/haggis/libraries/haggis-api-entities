package entities

import (
	"strings"

	"github.com/pkg/errors"
)

//AccountingGroup represents an HTCondor Accounting Group
type AccountingGroup struct {
	ID            int
	Name          string
	Description   string
	Parent        int
	ChargeToGroup int
}

//AccountingGroupExtended is a decorated Accounting Group
type AccountingGroupExtended struct {
	Group         *AccountingGroup
	Parent        *AccountingGroup
	ChargeToGroup *ChargeGroup
}

//IsChild tells if an accounting group is top level or not.
func (g AccountingGroup) IsChild() bool {
	if g.Parent == 0 {
		return false
	}

	return true
}

//Validate validates the correctness of an Accounting Group against a set of rules
func (g AccountingGroup) Validate() error {
	err := g.validateName()
	if err != nil {
		return err
	}

	return nil
}

func (g AccountingGroup) validateName() error {
	if !(strings.HasPrefix(g.Name, "group_u_")) {
		return errors.New("name must start with group_u_")
	}

	if strings.ContainsAny(g.Name, " ") {
		return errors.New("name must not contain white spaces")
	}

	return nil
}
