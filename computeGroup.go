package entities

import "github.com/pkg/errors"

//ComputeGroup represents a user group
type ComputeGroup struct {
	ID         int
	Name       string
	Type       string
	Accounting int
}

//ComputeGroupExtended is a decorated Compute Group
type ComputeGroupExtended struct {
	Group    *ComputeGroup
	MappedTo *AccountingGroup
}

//Validate validates the correctness of a Compute Group against a set of rules
func (g ComputeGroup) Validate() error {
	if !(g.Type == "unix") && !(g.Type == "egroup") {
		return errors.New("type must be unix or egroup")
	}
	return nil
}
