package entities

//Pool represents a resource pool
type Pool struct {
	ID          int
	Name        string
	Description string
}

//Validate validates the correctness of a pool against a set of rules
func (p Pool) Validate() error {
	return nil
}
