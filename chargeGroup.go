package entities

import (
	"strings"

	"github.com/pkg/errors"
)

var validCategories = []string{"experiment", "project", "fe", "department"}

// ChargeGroup represents an accounting charging group.
type ChargeGroup struct {
	ID       int
	Name     string
	UUID     string
	Category string
	Admins   []Admin
	Active   bool
}

// Admin holds information about the administrator of each charge group.
type Admin struct {
	Username string
	Email    string
}

//Validate validates the correctness of an Charge Group against a set of rules
func (g ChargeGroup) Validate() error {
	if !g.validateCategory() {
		return errors.New("wrong category")
	}
	return nil
}

func (g ChargeGroup) validateCategory() bool {
	for _, v := range validCategories {
		if strings.ToLower(g.Category) == v {
			return true
		}
	}
	return false
}
