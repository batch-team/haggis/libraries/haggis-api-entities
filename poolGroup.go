package entities

// PoolGroup represents a Pool to Group mapping.
type PoolGroup struct {
	Pool             int
	Group            int
	ChargeRole       string
	ChargeProperties int
	Resources        *Resources
}

func (PoolGroup) Validate() error {
	return nil
}

// PoolGroupExtended represents a decorated Pool to Group mapping.
type PoolGroupExtended struct {
	PoolGroup        *PoolGroup
	Pool             *Pool
	Group            *AccountingGroup
	ChargeProperties *ChargeProperties
}

// Resources represents PoolGroup resources.
type Resources struct {
	Quota    int
	Priority int
	Weight   int
	Surplus  bool
}

// Share is a mapping between groups and quotas. Used for rebalancing.
type Share struct {
	ID    int
	Quota int
}
